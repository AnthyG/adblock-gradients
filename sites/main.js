let blocked = 0;

const blockedAdd = (n = 0) => {
	if (n > 0) {
		blocked += n;
		console.debug("AdBlock-Gradients", blocked);
	}
};

function debounce(func, waitTime = 25) {
	let timeout;

	return function(...args) {
		clearTimeout(timeout);

		timeout = setTimeout(func.bind(func, ...args), waitTime);
	}
}
