const matchers = Object.values({
	en: ["Promoted"],
	de: ["Gesponsert", "Anzeige"],
	dk: ["Promoveret"],
	jp: ["プロモーション"],
	pt: ["Patrocinado"],
	se: ["Marknadsfört"],
	es: ["Promocionado"],
	nl: ["Gepromoot"],
	fr: ["Post sponsorisé"],
	it: ["Post sponsorizzato"],
	ru: ["Продвигается"],
	ch: ["广告"],
}).flat().map(matcher => `[aria-label~="${matcher}"]`).join(',');

const getItems = () =>
	new Promise(resolve => {
		const desktopItems = [...document.querySelectorAll(matchers)].map(
			$el => $el.closest(".feed-shared-update-v2 > div")
		);

		const mobileItems = [
			...document.querySelectorAll(
				'[data-is-sponsored]:not(.linkedblock-gradient-c)'
			)
		];

		const items = [...desktopItems, ...mobileItems];

		resolve(items);
	});

const remover = () => {
	getItems().then(items => {
		blockedAdd(items.length);

		items.forEach($el => {
			[...$el.children].forEach($el2 => $el2.remove());

			$el.classList.add("linkedblock-gradient-c");

			const $gradient = document.createElement("div");

			$gradient.classList.add(
				"linkedblock-gradient",
				randomGradient()
			);

			$el.appendChild($gradient);
		});
	});
};

(function () {
	console.debug("On a LinkedIn Domain!", matchers);

	window.addEventListener("scroll", debounce(remover));

	remover();
})();
